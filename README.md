![](https://churrops.files.wordpress.com/2017/06/ansible-logo.png?w=736)

# ADD GLOBAL ENVIRONMENT VARIABLE FOR LINUX

- settings hosts
- add host and playbook
- add file with the variables in the vars/main.yml path

### after running the playbook:

encrypt the vars / main.yml file with the following command:
~~~bash
$ ansible-vault encrypt roles/env/vars/main.yml
New Vault password:
Confirm New Vault password:
Encryption successful
~~~

after entering the password, the result is this [main.yml](roles/env/vars/main.yml)

so that they finish successfully you have to configure the variable with password used to encrypt the file.


There is no need to restart the server, variable is added automatically, but when the equipment is restarted, the variable is added automatically by profile.d


- don't forget to configure the export.sh file with your desired variable.

